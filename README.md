# requirements.script

small modular framework for handling installation of **project requirements** in `CI/CD` projects

# usage
1. add the requirements.sh script to your project's repository
2. add files corresponding to the type of dependencies required
3. modify the requirements definitions to match your needs
4. run the main `requirements.sh` script as pre-step in your CI/CD jobs
